# Project name
name: kde

# The minimum base BuildStream format
format-version: 14

# Where elements are stored
element-path: elements

# Store source refs in central project.refs file
# ref-storage: project.refs

# The uri to download built artifacts from for this project
# uncomment when we do have this working.
# artifacts:
#  url: URL_HERE

fatal-warnings:
- overlaps

# Options to specify for the project, these provide
# command line switches which control the behavior of
# conditional directives in the element.bst files.
#
options:
  arch:
    type: arch
    description: Machine architecture
    variable: arch
    values:
    - arm
    - aarch64
    - i686
    - x86_64
    - ppc64le


# Source aliases.
#
# These are used in the individual element.bst files in
# place of specifying full uris.
# 
# The location from where source code is downloaded can
# be changed without triggering a rebuild.
#
aliases:
  gnome_downloads: https://download.gnome.org/sources/
  cairo_org: https://cairographics.org/
  cdn_kernel_org: https://cdn.kernel.org/pub/
  downloads.sourceforge.net: https://downloads.sourceforge.net/
  ftp_gnu_org: https://ftp.gnu.org/gnu/
  git_code_sf_net: https://git.code.sf.net/
  git_freedesktop_org: https://gitlab.freedesktop.org/
  github_com: https://github.com/
  gitlab_com: https://gitlab.com/
  gitlab_gnome_org: https://gitlab.gnome.org/
  gnome_download: https://download.gnome.org/
  gstreamer: https://gstreamer.freedesktop.org/src/
  icon_theme_freedesktop_org: https://icon-theme.freedesktop.org/releases/
  iso_codes: https://pkg-isocodes.alioth.debian.org/downloads/
  libburnia: http://files.libburnia-project.org/releases/
  people_freedesktop_org: https://people.freedesktop.org/
  purism: https://source.puri.sm/
  qqwing: https://qqwing.com/
  sourceforge_net: https://sourceforge.net/projects/
  spice: https://www.spice-space.org/download/
  tarballs_needing_help: https://people.gnome.org/~mcatanzaro/tarballs-needing-help/
  webkitgtk_org: https://webkitgtk.org/releases/

mirrors:
- name: kernel_org
  aliases:
    ftp_gnu_org:
    - https://mirrors.kernel.org/gnu/

# Some overrides to the default sandbox execution environment
#
environment:
  LC_ALL: en_US.UTF-8
  LD_LIBRARY_PATH: '%{libdir}'
  PATH: /usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
  PKG_CONFIG_PATH: /usr/local/lib/pkgconfig:%{libdir}/pkgconfig
  CFLAGS: "%{flags}"
  CXXFLAGS: "%{flags}"
  LDFLAGS: "%{ldflags_defaults}"

# Some overrides to element configuration based on type
#
# Here we can set a project wide options for various build systems,
# e.g. we can add --disable-gtk-doc to every `./configure` line.
elements:
  autotools:
    variables:
      conf-global: --disable-static --disable-Werror --host=%{triplet} --build=%{triplet}

  cmake:
    variables:
      cmake-global: -DCMAKE_BUILD_TYPE=RelWithDebInfo
      generator: Ninja
  distutils:
    variables:
      python-install: '%{python} setup.py install --root "%{install-root}"'
      fix-pyc-timestamps: ''
    config:
      install-commands:
        (>):
          # There's no way to get the setup.py to install this in the right place.
        - |
          if [ -d '%{install-root}/usr/lib/pkgconfig/' ]; then
            mkdir -p %{install-root}/usr/%{lib}/
            mv -f %{install-root}/usr/lib/pkgconfig/ %{install-root}/usr/%{lib}/
          fi
  meson:
    variables:
      meson-global: --buildtype=plain --auto-features=enabled

  filter:
    config:
      include-orphans: true
  qmake:
    variables:
      qmake: qmake -makefile "QMAKE_CFLAGS+=$CFLAGS" "QMAKE_CXXFLAGS+=$CXXFLAGS" "QMAKE_LFLAGS+=$LDFLAGS" "CONFIG+=force_debug_info"

sources:
  git_tag:
    config:
      checkout-submodules: false
      track-tags: false

split-rules:
  devel:
    (>):
    - '%{indep-libdir}/**/include'
    - '%{indep-libdir}/**/include/**'
    - '%{bindir}/*-config'
    - '%{libdir}/cmake'
    - '%{libdir}/cmake/**'
    - '%{datadir}/cmake'
    - '%{datadir}/cmake/**'
    - '%{datadir}/gir-1.0'
    - '%{datadir}/gir-1.0/**'
    - '%{datadir}/vala*/vapi'
    - '%{datadir}/vala*/vapi/**'
  vm:
  - '%{datadir}/dbus-1/**'

# Define some behavior for `bst shell`
#
shell:

  # Default command for `bst shell`, we prefer bash, and disable
  # parsing of profile and rc files so that the prompt BuildStream
  # sets is not overwritten.
  command: ['bash', '--noprofile', '--norc', '-i']

  # Some environment variables to inherit from the host environment
  environment:
    LANG: '$LANG'
    DISPLAY: '$DISPLAY'
    DBUS_SESSION_BUS_ADDRESS: '$DBUS_SESSION_BUS_ADDRESS'
    XDG_RUNTIME_DIR: '$XDG_RUNTIME_DIR'

    # Inform applications which use pulseaudio of the server socket
    PULSE_SERVER: 'unix:${XDG_RUNTIME_DIR}/pulse/native'

  # Some things to mount into the sandbox
  host-files:
  # Understand user inherited uid/gid
  - '/etc/passwd'
  - '/etc/group'
  # Allow network resolution
  - '/etc/resolv.conf'

  # Allow access to plausible video devices,
  # declare these optional to avoid meaningless warnings
  - path: '/dev/dri'
    optional: true
  - path: '/dev/mali'
    optional: true
  - path: '/dev/mali0'
    optional: true
  - path: '/dev/umplock'
    optional: true
  - path: '/dev/nvidiactl'
    optional: true
  - path: '/dev/nvidia0'
    optional: true
  - path: '/dev/nvidia-modeset'
    optional: true

  # Allow access to sysfs, needed for local device discovery
  - '/sys'

  # Allow access to the user runtime directory, this
  # will include the pulseaudio socket along with some
  # other things.
  - '${XDG_RUNTIME_DIR}'

variables:
  branch: master
  branch-nice-name: Nightly #Should be %{branch} if not master
  installer-volume-id: "GNOME-OS-%{branch-nice-name}-%{arch}"
  qualifier: ''
  flatpak-branch: '%{branch}%{qualifier}'
  ostree-layer: user
  # ostree-branch: "kde-os/%{branch}/%{arch}-%{ostree-layer}"
  # I have no idea what to put here atm.
  # ostree-remote-url: "https://nightly.gnome.org/gnomeos/repo"
  gcc_arch: "%{arch}"
  abi: "gnu"
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{arch}-unknown-linux-%{abi}"
  lib: "lib/%{gcc_triplet}"
  sbindir: "%{bindir}"
  sysconfdir: "/etc"
  localstatedir: "/var"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  common_flags: "-O2 -g -pipe -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -fexceptions -fstack-protector-strong -grecord-gcc-switches"
  ldflags_defaults: "-Wl,-z,relro,-z,now -Wl,--as-needed -L%{libdir}"
  local_flags: ''

  (?):
  - arch == "i686":
      gcc_arch: "i386"
      flags: "-march=i686 -mtune=generic -msse2 -mfpmath=sse -mstackrealign %{common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection %{local_flags}"
  - arch == "arm":
      abi: "gnueabihf"
      flags: "%{common_flags} %{local_flags}"
  - arch == "ppc64le":
      gcc_arch: "powerpc64le"
  - arch == "x86_64":
      flags: "-march=x86-64 -mtune=generic %{common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection %{local_flags}"
  - arch == "aarch64":
      flags: "%{common_flags} -fasynchronous-unwind-tables -fstack-clash-protection %{local_flags}"
  - arch == "ppc64le":
      flags: "%{common_flags} -mcpu=power9 -mtune=power9 %{local_flags}"


  (@):
  - core/freedesktop-sdk.bst:include/strip.yml

plugins:
- origin: pip
  package-name: buildstream-external
  elements:
    flatpak_image: 0
    flatpak_repo: 0
    collect_manifest: 0
    x86image: 0
  sources:
    cargo: 0
    git_tag: 1
